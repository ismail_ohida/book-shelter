import React from 'react';

const User = ({name, email}) => {
    return (
        <div className="person">
            <h3>{name}</h3>
            <span>{email}</span>
        </div>
    );
};

export default User;