import SignUp from "./pages/auth/SignUp";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Login from "./pages/auth/Login";

const App = () => {
    return (
        <Router>
            <Switch>
                <Route path="/" component={Login}/>
                <Route path="/signup" component={SignUp}/>
            </Switch>
        </Router>
    );
}

export default App;
