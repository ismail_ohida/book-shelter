import React, {useState} from 'react';
import classes from "./SignUp.module.css";
import facebook_icon from "../../asset/images/fa4e9dbbd3b2b56831f6a6f4f308e59997e824e3.png";
import google_icon from "../../asset/images/85c686bacaee18509b576c15ce5e87caaac3223c.png";
import {Link} from "react-router-dom";

const SignUp = () => {

    const INITIAL_USER = {username: '', email:'', password: ''};

    const [user, setUser] = useState(INITIAL_USER);
    const [isValid, setIsValid] = useState(true);

    const newUserHandler = e =>{
        const {value, name} = e.target;
        if (value.trim().length > 0){
            setIsValid(true);
        }
        setUser(prevState => {
            return {...prevState, [name]: value};
        })
    }

    function validateUserInput (value) {
        return user[value].trim().length === 0;
    }

    function onLogin(user) {
        console.log(user);
    }

    const submitHandler = e =>{
        e.preventDefault();

        const values = Object.keys(user);
        for (const value of values) {
            if (validateUserInput(value)){
                setIsValid(false);
                return;
            }
        }
        onLogin(user);
        setUser(INITIAL_USER);
        setIsValid(true);
    }

    return (
        <div className={classes.container}>
            <div className={classes.eclipse}/>
            <div className={classes.eclipse}/>
            <div className={classes.wrapper}>
                <h3>Sign up with</h3>
                <div className={classes['media-btn']}>
                    <div className={classes.btn}>
                        <img src={google_icon} alt="google-icon"/>
                        <button type="button">google</button>
                    </div>
                    <div className={classes.btn}>
                        <img src={facebook_icon} alt="facebook-icon"/>
                        <button type="button">facebook</button>
                    </div>
                </div>
                <p>or</p>

                <form onSubmit={submitHandler} className={`${classes.form} ${isValid ? ".success" : "error"}`}>
                    <div className={classes['form-control']}>
                        <label htmlFor="username">username</label>
                        <input type="text" placeholder="Enter your username" name="username" onChange={newUserHandler}/>
                    </div>
                    <div className={classes['form-control']}>
                        <label htmlFor="email">email</label>
                        <input type="email" placeholder="Enter email address" name="email" onChange={newUserHandler}/>
                    </div>
                    <div className={classes['form-control']}>
                        <label htmlFor="password">password</label>
                        <input type="password" placeholder="Enter your password" name="password" onChange={newUserHandler}/>
                    </div>
                    <button type="submit">Sign up</button>
                </form>
                {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <span>Already have an account? <Link to="/">Log in</Link></span>
            </div>
        </div>
    );
};

export default SignUp;